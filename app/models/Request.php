<?php


namespace app\models;


use app\core\Model;

class Request extends Model
{
    public $id;
    public $name;
    public $desc;
    public $year;

    protected $table = 'requests';

    public function request()
    {
        $sql = "SELECT requests.id, offers.name AS ofname, requests.price, requests.count, operators.name
                  FROM `requests` 
                  LEFT JOIN `offers` 
                            ON requests.offer_id = offers.id
                  LEFT JOIN `operators` 
                            ON requests.operator_id = operators.id
                  WHERE requests.count > 2 
                            AND operators.id = 10 OR operators.id = 12";
        $r = $this->db->row($sql);
        return $r;
    }

    public function request2()
    {
        $sql = "SELECT offers.name, requests.count, requests.price 
                  FROM `requests` 
                  LEFT JOIN `offers`
                        ON requests.offer_id = offers.id
                  GROUP BY offers.name 
                  ORDER BY requests.count";
        $r = $this->db->row($sql);
        return $r;
    }

}