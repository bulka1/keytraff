<?php


namespace app\controllers;


use app\core\Controller;

class RequestController extends Controller
{

    public function index()
    {
        if (isset($_POST['request'])) {
            $result2 = $this->model->request2();
            $data = [
                'status'  => 'ok',
                'requests' => $result2,
            ];
            $data = json_encode($data);
            print_r($data);
        } else {
            $result = $this->model->request();

            $data = [
                'requests' => $result,
            ];
        }


        $this->view->render('Home Page', $data);
    }

}