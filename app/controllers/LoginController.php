<?php


namespace app\controllers;


use app\core\Controller;

class LoginController extends Controller
{
    public function index()
    {
        if (!empty($_POST)) {
            $this->view->location('/');
        }
        $this->view->path = 'auth/login';
        $this->view->render('Login');
    }

}