<div class="col-md-6 col-md-offset-3">
    <h3 style="text-align: center">Register</h3>
    <hr>
    <form class="" action="">
        <div class="form-group">
            <label for="name">Full Name</label>
            <input type="name" class="form-control" id="name">
        </div>
        <div class="form-group">
            <label for="email">Email address:</label>
            <input type="email" class="form-control" id="email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd">
        </div>

        <button type="submit" class="btn btn-info">Login</button>
    </form>
</div>