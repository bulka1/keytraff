<div class="col-md-6 col-md-offset-3">
  <h3 style="text-align: center">Login</h3>
  <hr>
  <form action="/login" method="post">
    <div class="form-group">
      <label for="email">Email address:</label>
      <input type="email" class="form-control" name="email">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" name="password">
    </div>

    <button type="submit" class="btn btn-info">Login</button>
  </form>
</div>