<h3>Test Work - KeyTraff</h3>


<div class="row">
  <div class="col-md-12">
    <button class="btn btn-primary" id="req2">Request #2</button>
    <hr>
    <table class="table table-striped">
      <thead>
      <tr>
        <th>id</th>
        <th>ofname</th>
        <th>name</th>
        <th>price</th>
        <th>count</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($data['requests'] as $request): ?>
        <tr>
          <td><?php echo $request['id']?></td>
          <td><?php echo $request['ofname']?></td>
          <td><?php echo $request['name']?></td>
          <td><?php echo $request['price']?></td>
          <td><?php echo $request['count']?></td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>

