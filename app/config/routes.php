<?php

return [
    '' => [
        'controller' => 'request',
        'action'     => 'index',
    ],

    'login' => [
        'controller' => 'login',
        'action'     => 'index',
    ],

    'register' => [
        'controller' => 'register',
        'action'     => 'index',
    ],

];