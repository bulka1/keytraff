<?php


use app\core\Router;

spl_autoload_register(function ($class_name) {
    $path = $class_name;

    if (file_exists( $path . '.php')){
        include $class_name . '.php';
    }

});

$router = new Router();
$router->run();

